`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    13:24:06 01/01/2019
// Design Name:
// Module Name:    add_round_key
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module add_round_key(
	input [127:0] key_in,
	input [7:0] state_in [15:0],
	output [7:0] state_out [15:0]
    );

	integer i;
	reg [7:0] key [15:0];

	always @(key_in) begin
		for (i = 0; i < 16; i = i + 1) begin
			key[i] = key_in[i * 8 + 7 : i * 8];
		end

		for (i = 0; i < 16; i = i + 1) begin
			state_out[i] = state_in[i] ^ key[i];
		end
	end

endmodule
