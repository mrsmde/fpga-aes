`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    13:29:39 01/01/2019
// Design Name:
// Module Name:    shift_row
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module shift_row(
	input [7:0] state_in [15:0],
	output [7:0] state_out [15:0]
    );

	integer i;
	integer j;
	
	always @(state_in) begin
		// Jesus Christ :B
		for (i = 0; i < 4; i = i + 1) begin
			for (j = 0; j < 4; j = j + 1) begin
				state_out [i * 4 + j] = state_in [i * 4 + ((i + j) % 4)];
			end
		end
	end

endmodule
