`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:54:52 01/08/2019
// Design Name:   expand_key
// Module Name:   C:/Users/pepe/Desktop/fpga-aes/expand_key_tb.v
// Project Name:  AES
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: expand_key
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module expand_key_tb;

	// Inputs
	reg [7:0] key_in;

	// Instantiate the Unit Under Test (UUT)
	expand_key uut (
		.key_in(key_in)
	);

	initial begin
		// Initialize Inputs
		key_in = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

